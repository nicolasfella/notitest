#include "notitest.h"
#include <QApplication>
#include <KNotification>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    notitest w;
    w.show();

    return app.exec();
}

