#ifndef NOTITEST_H
#define NOTITEST_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class notitest;
}

class notitest : public QMainWindow
{
    Q_OBJECT

public:
    explicit notitest(QWidget *parent = nullptr);
    ~notitest() override;

private:
    QScopedPointer<Ui::notitest> m_ui;
};

#endif // NOTITEST_H
