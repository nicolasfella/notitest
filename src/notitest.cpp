#include "notitest.h"
#include "ui_notitest.h"

#include <KNotification>
#include <QTimer>
#include <QApplication>

#include <QDebug>

#include <QDialog>

notitest::notitest(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::notitest)
{
    m_ui->setupUi(this);

	
	

    connect(m_ui->pushButton, &QPushButton::clicked, this, [this] {
	

        KNotification *n = new KNotification(QStringLiteral("event"), this);
        n->setTitle(QStringLiteral("Hello"));
        n->setText(QStringLiteral("Test"));
        n->setWindow(QGuiApplication::focusWindow());

        QTimer::singleShot(4000, n, &KNotification::sendEvent);
    });

	connect(m_ui->pushButton_2, &QPushButton::clicked, this, [this] {
	
	QTimer::singleShot(1000, this, [this]{
		qDebug() << "Kafooo";
// 		QApplication::alert(this);
        
        QDialog *dia = new QDialog(this);
        dia->show();
        
        QTimer::singleShot(1000, this, [this, dia] {
            QApplication::alert(dia);
        });
        
	});
	
    });

}

notitest::~notitest() = default;
